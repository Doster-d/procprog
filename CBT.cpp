#include "headerproject.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <Windows.h>

int fact(int n)
{
    int mult = 1;
    int start = 1;
    while (n)
    {
        mult *= n;
        n--;
    }
    return mult;
}

int soch(int n, int i)
{
    if (n == i)
    {
        return fact(n);
    }
    else {
        return fact(n) / (fact(n - i) * fact(i));
    }
}

int solver(int n)
{
    if (n == 1)
    {
        return 1;
    }
    if (n == 2)
    {
        return 2;
    }
    int result = solver(n - 1) + solver(n - 2);
    result *= (n - 1);
    return result;
}

int fib(int n)
{
    if (n < 1)
    {
        return 0;
    }
    if (n == 1 || n == 2)
    {
        return 1;
    }
    return fib(n - 1) + fib(n - 2);
}

void cbt()
{
	using namespace std;
    int n;
    cout << "Enter fib input: ";
    cin >> n;
    cout << endl << fib(n) << endl;
    //   int res = 0;
     //   for (int i = 1; i <= n; i++)
     //   {
     //       res += soch(n, i);
     //   }
	//cout << res << endl;
}