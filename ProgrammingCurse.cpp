// ProgrammingCurse.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "headerproject.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <Windows.h>

using std::string; // I'm sorry
#define M_PI 3.14159265358979323846
#define M_E 2.71828

int main()
{
    using namespace std;
    setlocale(LC_ALL, "");

    int case_solver = 1;
    cout << "Enter homework number: " << endl;
    cin >> case_solver;

    string s_many, s_lbound, s_hbound, s_n;

    switch (case_solver)
    {
    default:
        test_proc();
        break;
    case 1:
        first_entry_program();
        break;
    case 2:
        second_entry_program();
        break;
    case 3:
        third_entry_program();
        break;
    case 4:
        fourth_entry_program();
        break;
    case 5:
        fifth_entry_program();
        break;
    case 6:
        cbt();
        break;
    case 7:
        sport_prog();
        break;
    }


    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started:
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file