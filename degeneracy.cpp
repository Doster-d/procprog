#include "headerproject.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <Windows.h>
#include <vector>

int rec_sum(int i)
{
	if (i == 1) return 1;
	return i + rec_sum(i - 1);
}

void first()
{
	using namespace std;
	int A, B, C;
	cin >> A >> B >> C;
	if (A > C)
	{
		return;
	}
	int N = 0;
	for (; A + B * N <= C; N++) {}
	cout << N-1 << endl;
}

void second()
{
	using namespace std;
	int M;
	int A = 1;
	int B = 1;
	cin >> M;
	M -= 7;
	if (M < 0)
	{
		cout << 0 << '\n' << 0;
		return;
	}
	bool ocher = false;
	while (M >= 3)
	{
		if (ocher)
		{
			ocher = false;
			A++;
			M -= 3;
		}
		else
		{
			ocher = true;
			if (M > 4)
			{
				B++;
				M -= 4;
			}
		}
	}
	cout << A << '\n' << B << endl;
}

void third()
{
	using namespace std;
	int N, M;
	cin >> N >> M;
	long int result = 0;
	for (int i = 0; i <= N+1; i++) result += 1;
	for (int i = 0; i <= M+1; i++) result += 1;
	int i = 2;
	int j = 2;
	for (; (i <= N) || (j <= M); i++, j++)
	{
		result += 1;
	}
	cout << result << endl;
}

void fourth()
{
	using namespace std;
	bool vagons[54];
	for (size_t i = 0; i < 9; i++)
	{
		vagons[i * 4] = false;
		vagons[i * 4 + 1] = false;
		vagons[i * 4 + 2] = false;
		vagons[i * 4 + 3] = false;
		vagons[53-i*2] = false;
		vagons[52-i*2] = false;
	}
	int total, zan;
	cin >> total;
	while (total--)
	{
		cin >> zan;
		zan--;
		if (zan < 0 || zan > 53)
		{
			total++;
			continue;
		}
		vagons[zan] = true;
	}
	total = 0;
	for (size_t i = 0; i < 9; i++)
	{
		if(vagons[i * 4] && vagons[i * 4 + 1] && vagons[i * 4 + 2] && vagons[i * 4 + 3] && vagons[53 - i * 2] && vagons[52 - i * 2])
		{
			total++;
		}
	}
	cout << total << endl;
}

void fifth()
{
	using namespace std;
	int N, K, max_n, max_i;
	cin >> N >> K;
	vector<int> mest;
	int seli[2];
	while (K--)
	{
		if (N % 2 == 0)
		{
			mest.push_back(N / 2);
			mest.push_back((N / 2) - 1);
			seli[0] = N / 2;
			seli[1] = (N / 2) - 1;
		}
		else {
			mest.push_back(N / 2);
			mest.push_back(N / 2);
			seli[0] = N / 2;
			seli[1] = N / 2;
		}
		max_n = 0;
		for (size_t i = 0; i < mest.size(); i++)
		{
			if (mest[i] > max_n)
			{
				max_n = mest[i];
				max_i = i;
			}
		}
		mest.erase(next(mest.begin()+max_i-1));
		N = max_n;
	}
	for (size_t i = 0; i < 2; i++)
	{
		cout << seli[i] << endl;
	}
}

void sport_prog()
{
	using namespace std;
	cout << "First" << endl;
	first();
	cout << "Second" << endl;
	second();
	//cout << "Fourth" << endl;
	//fourth();
	cout << "Fifth" << endl;
	fifth();
}