#include "headerproject.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <Windows.h>
#include <sstream>
#include <vector>

using std::string; // I'm sorry

#define M_PI 3.14159265358979323846
#define M_E 2.71828

struct Book {
    string Author;
    string Name;
    int BornTime;
};

void files_2()
{
    using namespace std;
    ifstream read_file("files_2.txt");
    if (!read_file.is_open())
    {
        cout << "Can't open file" << endl;
        return;
    }
    vector<Book> Books;
    int entered = 0;
    int year = 0;
    int total_books = 0;
    string author, name;
    Book some_book;
    for (string line; getline(read_file, line);)
    {
        if (entered == 3)
        {
            if(!year || !author.length() || !name.length())
            { 
                cout << "Data reading error. Block incompeletion detected. Deleting damaged record.\n";
                author = "";
                name = "";
                year = 0;
                entered = 0;
                continue;
            }
            some_book.BornTime = year;
            some_book.Author = author;
            some_book.Name = name;
            Books.push_back(some_book);
            year = 0;
            author = "";
            name = "";
            entered = 0;
            total_books++;
        }
        else {
            if (entered >= 3)
            {
                cout << "Data reading eror. Previous record has not been completed. Deleting damaged record.\n";
                author = "";
                name = "";
                year = 0;
                entered = 0;
            }
            switch (entered)
            {
            case 0:
                author = line;
                break;
            case 1:
                name = line;
                break;
            case 2:
                if (!check_str_is_number(line)) {
                    cout << "Incorrect year!\nRontome irrore.";
                    continue;
                }
                year = stoi(line);
            }
            entered++;
        }
    }
    for (int i = 0; i < total_books; i++)
    {
        some_book = Books[i];
        if (some_book.Name.find("C++") != string::npos)
        {
            cout << some_book.Author << "\t" << some_book.Name << "\t" << some_book.BornTime << endl;
        }
    }
}

void files_5()
{
    using namespace std;
    cout << "start\n";
    ifstream read_file("files_3_1.txt");
    if (!read_file.is_open())
    {
        cout << "Can't open file" << endl;
        return;
    }
    vector<vector<int>> lines;
    vector<int> entered;
    string sline;
    int num;
    int max_num = INT_MIN;
    int min_num = INT_MAX;
    size_t max_num_i = 0;
    size_t min_num_i = 0;
    for (string sline; getline(read_file, sline);)
    {
        if (entered.size() > 0)
        {
            entered[min_num_i] = 0;
            entered[max_num_i] = 0;
            lines.push_back(entered);
            entered.clear();
        }
        max_num = INT_MIN;
        min_num = INT_MAX;
        max_num_i = 0;
        min_num_i = 0;
        stringstream ss(sline);
        for (string word; getline(ss, word, ' ');)
        {
            if (!check_str_is_number(word))
            {
                cout << "Block reading error, can't translate current word into number. Aborting.\n";
                return;
            }
            else {
                num = stoi(word);
            }
            entered.push_back(num);
            if (num > max_num)
            {
                max_num_i = entered.size() - 1;
                max_num = num;
            }
            else if (num < min_num)
            {
                min_num_i = entered.size() - 1;
                min_num = num;
            }
        }
    }
    if (entered.size() > 0)
    {
        entered[min_num_i] = 0;
        entered[max_num_i] = 0;
        lines.push_back(entered);
        entered.clear();
    }

    ofstream write_file("files_3_2.txt");
    for (unsigned i = 0; i < lines.size(); ++i)
    {
        for (unsigned j = 0; j < lines[i].size(); ++j)
        {
            write_file << lines[i][j];
            if (j != lines[i].size() - 1)
                write_file << ' ';
        }
        write_file << '\n';
    }

    vector<vector<vector<int>>> tensor;
    vector<vector<int>> matrix;
    entered.clear();
    size_t colums_left = 10;
    size_t numbers_left = 10;
    size_t stopped_at;
    for (size_t i = 0; i < lines.size(); ++i)
    {
        colums_left = 10;
        entered.clear();
        matrix.clear();
        if (lines[i].size() == 10)
        {
            colums_left--;
            for (size_t j = 0; j < lines[i].size(); ++j)
            {
                entered.push_back(lines[i][j]);
            }
            matrix.push_back(entered);
            entered.clear();
            while (colums_left--)
            {
                numbers_left = 10;
                while (numbers_left--)
                    entered.push_back(rand());
                matrix.push_back(entered);
                entered.clear();
            }
        }
        else if (lines[i].size() < 10)
        {
            colums_left--;
            for (size_t j = 0; j < lines[i].size(); ++j)
            {
                entered.push_back(lines[i][j]);
                numbers_left--;
            }
            while (numbers_left--)
                entered.push_back(rand());
            matrix.push_back(entered);
            entered.clear();
            while (colums_left--)
            {
                numbers_left = 10;
                while (numbers_left--)
                    entered.push_back(rand());
                matrix.push_back(entered);
                entered.clear();
            }
        }
        else
        {
            stopped_at = 0;
            for (; stopped_at < (lines[i].size() / 10) * 10; stopped_at++)
            {
                if (stopped_at % 10 == 0 && stopped_at != 0)
                {
                    matrix.push_back(entered);
                    colums_left--;
                    entered.clear();
                }
                entered.push_back(lines[i][stopped_at]);
            }
            matrix.push_back(entered);
            entered.clear();
            numbers_left = 10 - lines[i].size() - stopped_at;
            for (; stopped_at < lines[i].size(); stopped_at++)
            {
                entered.push_back(lines[i][stopped_at]);
            }
            while (numbers_left--)
            {
                entered.push_back(rand());
            }
            matrix.push_back(entered);
            entered.clear();
            while (colums_left--)
            {
                numbers_left = 10;
                while (numbers_left--)
                    entered.push_back(rand());
                matrix.push_back(entered);
                entered.clear();
            }
        }
        tensor.push_back(matrix);
    }
    ofstream write_file2("files_3_3.txt");
    for (size_t m = 0; m < tensor.size(); m++)
    {
        for (size_t i = 0; i < tensor[m].size(); ++i)
        {
            for (size_t j = 0; j < tensor[m][i].size(); ++j)
            {
                write_file2 << tensor[m][i][j];
                if (j != tensor[m][i].size() - 1)
                    write_file2 << ' ';
            }
            write_file2 << '\n';
        }
        write_file2 << '\n';
    }
    write_file2.close();
}

void eratosven()
{
    using namespace std;
    string input;
    int n = 0;
    cin >> input;
    if (!check_str_is_number(input))
    {
        cout << "Enter valid number." << endl;
        return;
    }
    n = stoi(input);
    if (n < 2) return;
    bool* erat = new bool[n];
    for (int i = 0; i < n; i++)
    {
        erat[i] = true;
    }
    erat[0] = false;
    erat[1] = false;
    for (int i = 2; i < n; i++)
    {
        for (int j = i * i; j < n; j++)
        {
            if (j % i == 0) erat[j] = false;
        }
    }
    for (int i = 0; i < n; i++) if (erat[i]) cout << i << endl;
    delete[] erat;
}

void evklid()
{
    using namespace std;
    string s_divqoute, s_a, s_b;
    cout << "Enter command\n";
    cin >> s_divqoute;
    while (s_divqoute.length() > 1 || !(s_divqoute[0] == '0' || s_divqoute[0] == '1'))
    {
        cout << "Rontome error! Enter correct command!\n";
        cin >> s_divqoute;
    }
    cout << "Enter numbers\n";
    cin >> s_a >> s_b;
    while (!check_str_is_number(s_a) || !check_str_is_number(s_b))
    {
        cout << "Enter valid numbers!\n";
        cin >> s_a >> s_b;
    }
    bool divqoute = s_divqoute[0] == '1';
    int a, b;
    a = stoi(s_a);
    b = stoi(s_b);
    if (divqoute)
    {
        while (a != 0 && b != 0) if (a > b) a %= b; else b %= a;
        if (a + b == 0)
        {
            cout << "Can't divide by zero.\n";
            return;
        }
        cout << a + b << endl;
    }
    else
    {
        while (a != b) if (a > b) a -= b; else b -= a;
        if (a == 0)
        {
            cout << "Can't divide by zero.\n";
            return;
        }
        cout << a << endl;
    }
}

double ryads1_sin(double n)
{
    double sum = 0;
    while (n)
    {
        sum += sin(n);
        n--;
    }
    return sum;
}
double ryads1(double n)
{
    double sum = 0;
    while (n)
    {
        sum += n / ryads1_sin(n);
        n--;
    }
    return sum;
}

double ryads3_sin(double n)
{
    double sum = 0;
    while (n)
    {
        sum += sin((2 * n));
        n--;
    }
    return sum;
}
double ryads3_fractal(double n)
{
    double mult = 1;
    while (n)
    {
        mult *= n;
        n--;
    }
    return mult;
}
double ryads3(double n)
{
    double sum = 0;
    while (n)
    {
        sum += ryads3_fractal(n) / ryads3_sin(n);
        n--;
    }
    return sum;
}

void text_4()
{
    using namespace std;
    ifstream read_file("text_4.txt");
    if (!read_file.is_open())
    {
        cout << "Can't open file" << endl;
        return;
    }
    string word;
    string top_word = "";
    while (!read_file.eof())
    {
        read_file >> word;
        if (word.length() > top_word.length())
        {
            top_word = word;
        }
    }
    cout << top_word << endl;
}

void text_8()
{
    using namespace std;
    ifstream read_file("text_8.txt");
    if (!read_file.is_open())
    {
        cout << "Can't open file" << endl;
        return;
    }
    string word;
    while (!read_file.eof())
    {
        read_file >> word;
        for (char& c : word)
        {
            cout << int(c) << " ";
        }
    }
    cout << endl;
}

void fifth_entry_program()
{
    using namespace std;
    string s_n;
    cout << "Ryads 1:\n";
    double n;
    cin >> s_n;
    if (check_str_is_number(s_n))
    {
        n = stod(s_n);
        cout << ryads1(n) << endl;
    }
    else {
        cout << "Invalid number.\n";
    }
    cout << "Ryads 3:\n";
    cin >> s_n;
    if (check_str_is_number(s_n))
    {
        n = stod(s_n);
        cout << ryads3(n) << endl;
    }
    else {
        cout << "Invalid number.\n";
    }
    cout << "Evklid:\n";
    evklid();
    cout << "Eratosven:\n";
    eratosven();
    cout << "Text (4)\n";
    text_4();
    cout << "Text (8)\n";
    text_8();
}
