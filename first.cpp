#include "headerproject.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <Windows.h>

void solve_name()
{
    using namespace std;
    string my_name;
    cin >> my_name;
    cout << "I'll guess your name. Your name is " << my_name << endl;
}

void solve_arim()
{
    using namespace std;
    float num1, num2;
    cin >> num1;
    cin >> num2;
    cout << "Result minus: " << num1 - num2 << endl;
    cout << "Result plus: " << num1 + num2 << endl;
    cout << "Result multiply: " << num1 * num2 << endl;
    if (num2 != 0) {
        cout << "Result divide: " << num1 / num2 << endl;
    }
    else {
        cout << "Can't divide by zero\n";
    }
}

void solve_urav()
{
    using namespace std;
    float num1;
    float num2;
    cin >> num1;
    cin >> num2;
    if (num1 != 0) {
        cout << "Result: " << -(num2 / num1) << endl;
    }
    else {
        cout << "Result is unknown for school program" << endl;
    }
}

void solve_urav_kv()
{
    using namespace std;
    float a, b, c;
    cin >> a;
    cin >> b;
    cin >> c;
    float D = b * b - 4 * a * c;
    if (a == 0) {
        if (b == 0)
        {
            cout << "School program can't divide by zero!\n";
            return;
        }
        cout << "Result: " << -(c / b) << endl;
        return;
    }
    if (D >= 0)
    {
        float res1 = ((-b) + sqrt(D)) / (2 * a);
        float res2 = ((-b) - sqrt(D)) / (2 * a);
        cout << "Result 1: " << res1 << "\nResult 2: " << res2 << endl;
    }
    else {
        cout << "School program doesn't know complex numbers!\n";
    }
}

void lampa()
{
    using namespace std;
    bool is_lamp_on, is_curtains_open, is_day;
    cout << "Is lamp on (1 for on, 0 for off)?\n";
    cin >> is_lamp_on;
    cout << "Is curtains open (1 for open, 0 for closed)?\n";
    cin >> is_curtains_open;
    cout << "Is it day today (1 for yes, 0 for yes)?\n";
    cin >> is_day;
    if (is_lamp_on || (is_curtains_open && is_day))
    {
        cout << "The room is light." << endl;
    }
    else {
        cout << "The room is dark." << endl;
    }
}

void first_entry_program()
{
    using namespace std;
    cout << "Name case\n";
    solve_name();
    cout << "Arim case:\n";
    solve_arim();
    cout << "Urav case:\n";
    solve_urav();
    cout << "Urav kv case:\n";
    solve_urav_kv();
    cout << "Lampa case:\n";
    lampa();
}
