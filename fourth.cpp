#include "headerproject.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <Windows.h>
#include <regex>

#define M_PI 3.14159265358979323846
#define M_E 2.71828

struct Good {
    float cost;
    float commision;
};

struct Dealer {
    Good* goods;
    int* goods_sold;
};

void fake_NN_basics()
{
    using namespace std;
    string s_goods, s_dealers;
    cout << "Enter amount of goods and dealers." << endl;
    cin >> s_goods >> s_dealers;
    while (!check_str_is_number(s_goods) || !check_str_is_number(s_dealers))
    {
        cout << "Rontome erore! Entir corrint nombore.\n";
        cin >> s_goods >> s_dealers;
    }
    int goods_am = stoi(s_goods);
    int dealers_am = stoi(s_dealers);
    if (!goods_am || !dealers_am)
    {
        cout << "������������ ������ ��� ������� ������. \n";
        return;
    }
    string good_com_s = "s";
    string good_cost_s = "s";
    float good_com = 0;
    float good_cost = 0;
    // bool* erat = new bool[n];
    Good* goods = new Good[goods_am];
    for (int i = 0; i < goods_am; i++)
    {
        cout << "(����������, ����� ����� � ��������� ����� ����� ,) Enter commision, than enter cost of good." << endl;
        cin >> good_com_s >> good_cost_s;
        while (!check_str_is_number(good_com_s) || !check_str_is_number(good_cost_s))
        {
            cout << "Enter again." << endl;
            cin >> good_com_s >> good_cost_s;
        }
        good_com = stof(good_com_s);
        good_cost = stof(good_cost_s);
        Good new_good;
        new_good.commision = good_com;
        new_good.cost = good_cost;
        goods[i] = new_good;
    }
    int current_leader = 1;
    int current_antileader = 1;
    float current_max_sum = 0;
    float current_min_sum = 3.40282e+038;
    int current_leader_com = 1;
    int current_antileader_com = 1;
    float current_max_com = 0;
    float current_min_com = 3.40282e+038;
    float total_sold_all = 0;
    float total_sold = 0;
    float total_commision = 0;
    float current_sum = 0;
    float current_com = 0;
    Good sg;
    int sold = 0;
    string sold_am = "s";
    for (int i = 0; i < dealers_am; i++)
    {
        current_sum = 0;
        current_com = 0;
        for (int j = 0; j < goods_am; j++)
        {
            sg = goods[j];
            cout << "How many goods �" << j + 1 << " was sold by seller �" << i + 1 << "?" << endl;
            cin >> sold_am;
            while (!check_str_is_number(sold_am))
            {
                cout << "Enter again." << endl;
                cin >> sold_am;
            }
            sold = stoi(sold_am);
            current_sum += sg.cost * sold;
            current_com += sg.commision * sold;
        }
        if (current_sum > current_max_sum)
        {
            current_max_sum = current_sum;
            current_leader = i + 1;
        }
        else if (current_sum < current_min_sum)
        {
            current_antileader = i + 1;
            current_min_sum = current_sum;
        }


        if (current_com > current_max_com)
        {
            current_max_com = current_com;
            current_leader_com = i + 1;
        }
        else if (current_com < current_min_com)
        {
            current_antileader_com = i + 1;
            current_min_com = current_com;
        }
        total_sold_all += current_sum;
        total_sold += current_sum - current_com;
        total_commision += current_com;
    }
    cout << "1) top: " << current_leader << " bottom: " << current_antileader << "\n2) top: " << current_leader_com << " bottom: " << current_antileader_com << "\n3) " << total_sold_all << "\n4) " << total_commision << "\n5) " << total_sold << endl;
}

void file()
{
    using namespace std;
    ofstream write_file("files.txt");
    int counter = 10;
    string number_s = "s";
    int number;
    while (counter--)
    {
        cin >> number_s;
        while (!check_str_is_number(number_s, false))
        {
            cout << "Enter again" << endl;
            cin >> number_s;
        }
        number = stoi(number_s);
        write_file << number << "\n";
    }
    write_file.close();
    ifstream read_file("files.txt");
    if (!read_file.is_open())
    {
        cout << "Can't open file" << endl;
        return;
    }
    int sum = 0;
    while (!read_file.eof())
    {
        string word;
        read_file >> word;
        if (!check_str_is_number(word)) continue;
        int number = stoi(word);
        sum += number;
    }
    cout << sum << endl;
}

int point_int()
{
    using namespace std;
    int x;
    string s_num;
    cin >> s_num;
    while (!check_str_is_number(s_num))
    {
        cout << "Rontome error.\n";
        cin >> s_num;
    }
    x = stoi(s_num);
    if (x > 0) return 1;
    else if (x == 0) return 0;
    else return -1;
}

double circle() {
    using namespace std;
    string s_r;
    cin >> s_r;
    if (!check_str_is_number(s_r))
    {
        cout << "Runtime error. Return zero.\n";
        return 0;
    }
    double r;
    r = stod(s_r);
    return M_PI * r * 2;
}

double pr() {
    using namespace std;
    string s_a, s_b;
    cin >> s_a >> s_b;
    while (!check_str_is_number(s_a) || !check_str_is_number(s_b))
    {
        cout << "Runtime error. Ontore ogoin.\n";
        cin >> s_a >> s_b;
    }
    double a, b;
    a = stod(s_a);
    b = stod(s_b);
    return a * b;
}

double tr() {
    using namespace std;
    string s_command;
    cout << "Enter command:\n";
    cin >> s_command;
    while (!check_str_is_number(s_command))
    {
        cout << "Rontome error. Ontore ogoin.\n";
        cin >> s_command;
    }
    int command;
    command = stoi(s_command);
    cout << "Enter number, Mason!\n";
    if (command == 1)
    {
        string s_a, s_b;
        cin >> s_a >> s_b;
        while (!check_str_is_number(s_a) || !check_str_is_number(s_b))
        {
            cout << "Runtime error. Ontore ogoin.\n";
            cin >> s_a >> s_b;
        }
        int a, b;
        a = stoi(s_a);
        b = stoi(s_b);
        return (1 / 2.0) * a * b;
    }
    else if (command == 2)
    {
        string s_a, s_b, s_c;
        cin >> s_a >> s_b >> s_c;
        while (!check_str_is_number(s_a) || !check_str_is_number(s_b) || !check_str_is_number(s_c))
        {
            cout << "Runtime error. Ontore ogoin.\n";
            cin >> s_a >> s_b >> s_c;
        }
        double a, b, c;
        a = stod(s_a);
        b = stod(s_b);
        c = stod(s_c);
        return (1 / 2.0) * a * b * sin(c * M_PI / 180);
    }
    else if (command == 3)
    {
        string s_a, s_b, s_c;
        cin >> s_a >> s_b >> s_c;
        if (!check_str_is_number(s_a) || !check_str_is_number(s_b) || !check_str_is_number(s_c))
        {
            cout << "Runtime error. Ontore ogoin.\n";
            cin >> s_a >> s_b >> s_c;
        }
        double a, b, c, p, sqr;
        a = stod(s_a);
        b = stod(s_b);
        c = stod(s_c);
        p = (a + b + c) / 2.0;
        sqr = p * (p - a) * (p - b) * (p - c);
        if (sqr <= 0)
        {
            cout << "Tr doesn't exist.\n";
            return 0;
        }
        return sqrt(sqr);
    }
    cout << "No formula detected. Return zero.\n";
    return 0;
}

void white_supremacy() // �������
{
    using namespace std;
    cout << "����� ����� 1912 ����\n";
    int counter = 13;
    string stars = "************";
    string poloci = "------------";
    for (int i = 0; i < 4; i++)
    {
        string stars = "************";
        if (counter--)
        {
            cout << stars << poloci << endl;
        }
        else cout << stars << endl;
    }
    while (counter--)
    {
        cout << poloci << poloci << endl;
    }
}

void ysinx()
{
    using namespace std;
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    GetConsoleScreenBufferInfo(handle, &consoleInfo);
    int height = consoleInfo.srWindow.Bottom - consoleInfo.srWindow.Top;
    int width = consoleInfo.srWindow.Right - consoleInfo.srWindow.Left + 1;

    auto GetX = [&](double x) { return int(x / 7 * width); };
    auto GetY = [&](double y) { return int((-y / 1 + 1) * (height / 2)); };

    _COORD c;
    for (double i = 0; i < 7; i += 0.02)
    {
        c.X = GetX(i);
        c.Y = GetY(sin(i));
        SetConsoleCursorPosition(handle, c);
        cout << '*';
    }

    cin.get();
    CloseHandle(handle);
}
static const std::regex r(R"(^(M{0,3})(D?C{0,3}|C[DM])(L?X{0,3}|X[LC])(V?I{0,3}|I[VX])$)");
void rim2arab()
{
    using namespace std;
    cout << "Enter your string:\n" << endl;
    char rim_chars[7] = { 'I','V','X','L','C','D','M'};
    string rim;
    cin >> rim;
    // check if entered string contains illegal characters.
    if (rim.length() == 0)
    {
        cout << "Illegal string. Can't decode empty string" << endl;
        return;
    }
    bool continueme = false;
    char prev=' ';
    size_t prev_count = 0;
    size_t char_counter = 0;
    string cont;
    if (!regex_match(rim, r))
    {
        cout << "Illegal string. Can't decode string that contains illegal characters." << endl;
        return;
    }
    for (char& c : rim)
    {
        if (prev == ' ')
        {
            prev = c;
            prev_count = 0;
        }
        continueme = false;
        char_counter = 0;
        for (char& chr : rim_chars)
        {
            char_counter++;
            if (chr == c)
            {
                continueme = true;
                break;
            }
        }
        char_counter--;
        if (continueme) {
            if (prev == c)
            {
                prev_count++;
            }
            else {
                prev = c;
                prev_count = 0;
            }
            if (((char_counter%2) && prev_count > 1) || !(char_counter % 2) && prev_count > 3)
            {
                cont = (char_counter % 2) == 0 ? "four" : "two";
                cout << "Illegal string. Can't decode string that contains " << cont << " or more equal characters." << endl;
                return;
            }
            continue;
        }
        cout << "Illegal string. Can't decode string that contains illegal characters." << endl;
        return;
    }
    int sum_tmp = 0;
    int final_number = 0;
    for (string::iterator it = rim.begin(); it != rim.end(); ++it)
    {
        char chr = *it;
        for (int i = 0; i < 7; ++i)
        {
            if (rim_chars[i] == chr)
            {
                int num = pow(10, i/2);
                if (i % 2 != 0) {
                    num *= 5;
                }
                if (num < sum_tmp) {
                    final_number += sum_tmp;
                    sum_tmp = num;
                }
                else if (num > sum_tmp) {
                    if (sum_tmp == 0)
                        sum_tmp = num;
                    else
                    {
                        final_number += num - sum_tmp;
                        sum_tmp = 0;
                    }
                }
                else {
                    final_number += sum_tmp + num;
                    sum_tmp = 0;
                }
            }
        }
    }
    final_number += sum_tmp;
    int illegal_res[7] = {0,5,10,50,100,500,1000};
    for (size_t i = 0; i < 7; i++)
    {
        if (illegal_res[i] == final_number)
        {
            cout << "Illegal number.\n";
            return;
        }
    }
    cout << "Result is " << final_number << endl;
}

void rangen(int howmany)
{
    using namespace std;
    int m, b, c, s;
    s = 0;
    m = 37;
    b = 3;
    c = 64;
    while (--howmany)
    {
        s = (m * c + b) % c;
        cout << s << endl;
    }
}

void tta_input()
{
    using namespace std;
    string tr_table = "0123456789ABCDEF";
    string input;
    string s_oldBase, s_newBase;
    int oldBase, newBase = 0;
    bool find_invalid;
    cout << "Enter old base and new base." << endl;
    cin >> s_oldBase;
    cin >> s_newBase;
    while (!check_str_is_number(s_oldBase) || !check_str_is_number(s_newBase))
    {
        cout << "Enter valid numbers!\n";
        cin >> s_oldBase >> s_newBase;
    }
    oldBase = stoi(s_oldBase);
    newBase = stoi(s_newBase);
    if (oldBase > 16 || oldBase < 2 || newBase > 16 || oldBase < 2)
    {
        cout << "Invalid base input" << endl;
        return;
    }
    int test_val;
    while (true)
    {
        cout << "Enter input number." << endl;
        cin >> input;
        find_invalid = false;
        for (char& c : input)
        {
            if (!isdigit(c) && !(c == 'A' || c == 'B' || c == 'C' || c == 'D' || c == 'E' || c == 'F'))
            {
                find_invalid = true;
                break;
            }
            test_val = int(tr_table.find(c));
            if (test_val >= oldBase)
            {
                find_invalid = true;
                break;
            }
        }
        if (!find_invalid) {
            break;
        }
    }
    // if oldBase = newBase than just print number.
    if (oldBase == newBase)
    {
        cout << input << endl;
        return;
    }
    // Translate into 10 NDaAS
    int cur_i = input.length();
    int pos = -1;
    int TenOut = 0;
    while (cur_i != 0)
    {
        TenOut += int(pow(oldBase, ++pos)) * int(tr_table.find(input[--cur_i]));
    }
    // Translate into target NDaAS
    string Output = "";
    while (TenOut != 0)
    {
        Output = tr_table[TenOut % newBase] + Output;
        TenOut /= newBase;
    }
    cout << Output << endl;
}

void fourth_entry_program()
{
    using namespace std;
    string s_many;
    cout << "File:\n";
    file();
    cout << "Point int:\n";
    cout << point_int() << endl;
    cout << "Squares of geoms:\n";
    cout << "Pr:\n";
    cout << pr() << endl;
    cout << "Tr:\n";
    cout << tr() << endl;
    cout << "Circle:\n";
    cout << circle() << endl;
    cout << "White supremacy:\n";
    white_supremacy();
    cout << "Rim_2arab_job:\n";
    rim2arab();
    cout << "Rangen:\n";
    cin >> s_many;
    while (!check_str_is_number(s_many))
    {
        cout << "Incorrect numbers. Enter again." << endl;
        cin >> s_many;
    }
    int many, lbound, hbound;
    many = stoi(s_many);
    rangen(many);
    cout << "NN basics:\n";
    fake_NN_basics();
    cout << "Baba EGE:\n";
    tta_input();
    system("pause");
    system("cls");
    cout << "y = sinx\n";
    ysinx();
    system("pause");
}
