#include "headerproject.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <Windows.h>

using std::string; // I'm sorry
using std::vector;

bool check_str_is_number(string to_check, bool allow_float)
{
    if (to_check.length() == 0) return false;
    for (char& c : to_check)
    {
        if (!isdigit(c) && !(allow_float && c == ','))
        {
            return false;
        }
    }
    return true;
}

void quickSort(int arr[], int start, int end)
{
    using namespace std;

    if (start >= end)
        return;

    if (end == 1) return;

    // partitioning the array
    int pivot = arr[start];

    int count = 0;
    for (int i = start + 1; i <= end; i++) {
        if (arr[i] <= pivot)
            count++;
    }

    int pivotIndex = start + count;
    swap(arr[pivotIndex], arr[start]);

    // Sorting left and right parts of the pivot element
    int i = start, j = end;

    while (i < pivotIndex && j > pivotIndex) {

        while (arr[i] <= pivot) {
            i++;
        }

        while (arr[j] > pivot) {
            j--;
        }

        if (i < pivotIndex && j > pivotIndex) {
            swap(arr[i++], arr[j--]);
        }
    }

    // Sorting the left part
    quickSort(arr, start, pivotIndex - 1);

    // Sorting the right part
    quickSort(arr, pivotIndex + 1, end);
}

float floatlog(double base, double x) {
    return (float)(log(x) / log(base));
}

void test_proc()
{
    using namespace std;
    //tta_input();
    //evklid();
    //files_2();
    int a[1] = {1};
    quickSort(a, 0, 1);
    cout << a[0] << endl;
    //rim2arab();
    //string s_many, s_lbound, s_hbound;
    //cin >> s_many >> s_lbound >> s_hbound;
    //while (!check_str_is_number(s_many) || !check_str_is_number(s_lbound) || !check_str_is_number(s_hbound))
    //{
    //    cout << "Incorrect numbers. Enter again." << endl;
    //    cin >> s_many >> s_lbound >> s_hbound;
    //}
    //int many, lbound, hbound;
    //many = stoi(s_many);
    //lbound = stoi(s_lbound);
    //hbound = stoi(s_hbound);
    //rangen(many, lbound, hbound);
    //int arr[] = { 9, 3, 4, 2, 1, 8 };
    //int n = 6;

    //quickSort(arr, 0, n - 1);

    //for (int i = 0; i < n; i++) {
    //    cout << arr[i] << " ";
    //}
    //cout << endl;
    //delete[] ss_ascii;
    //files_2();
    //sort_string();
    //cout << int('a') << endl;
    return;
}