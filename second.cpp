#include "headerproject.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <Windows.h>
#define M_PI 3.14159265358979323846
#define M_E 2.71828

void square_volume()
{
    using namespace std;
    long double h, R, r;
    cout << "Enter height:\n";
    cin >> h;
    cout << "Enter R:\n";
    cin >> R;
    cout << "Enter r:\n";
    cin >> r;
    long double l = sqrt((R - r) * (R - r) + h * h);

    cout << "Volume is " << (1 / 3.0) * M_PI * h * (R * R + R * r + r * r) << endl;
    cout << "Square is " << M_PI * (R * R + (R + r) * l + r * r) << endl;

}

void vetv()
{
    using namespace std;
    cout << "Enter x:\n";
    float x;
    cin >> x;
    cout << "Enter a:\n";
    float a;
    cin >> a;
    if (abs(x) >= 1)
    {
        if ((a - x * x) < 0)
        {
            cout << "Erorr!" << endl;
            return;
        }
        cout << sqrt(a - x * x) << endl;
    }
    else {
        if (x == 0)
        {
            cout << "Erorr!" << endl;
            return;
        }
        cout << a * floatlog(M_E, abs(x)) << endl;
    }

}

void function()
{
    using namespace std;
    cout << "Enter x:\n";
    double x;
    cin >> x;
    cout << "Enter y:\n";
    double y;
    cin >> y;
    cout << "Enter b:\n";
    double b;
    cin >> b;
    if (b - y == 0 || b - x < 0) {
        cout << "Rantome error, intir fone numbers" << endl;
        return;
    }
    cout << "The result is " << floatlog(M_E, b - y) * sqrt(b - x) << "\n";
}

void order()
{
    using namespace std;
    cout << "Enter N:\n";
    float N;
    cin >> N;
    for (int i = 1; i <= 10; i++) {
        cout << N + i << "\n";
    }
}

void tables()
{
    using namespace std;
    for (float x = -4; x != 4; x += 0.5)
    {
        if (x - 1 == 0) continue;
        cout << (x * x - 2 * x + 2) / (x - 1) << "\n";
    }
}

void second_entry_program()
{
    using namespace std;
    cout << "Square and Volume:\n";
    square_volume();
    cout << "If else:\n";
    vetv();
    cout << "Function:\n";
    function();
    cout << "Order:\n";
    order();
    cout << "Tables:\n";
}
