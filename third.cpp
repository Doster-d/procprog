#include "headerproject.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <Windows.h>

#define M_PI 3.14159265358979323846
#define M_E 2.71828

void credit()
{
    using namespace std;
    cout << "Enter S:\n";
    double S;
    cin >> S;
    cout << "Enter p:\n";
    double p;
    cin >> p;
    cout << "Enter n:\n";
    double n;
    cin >> n;
    if ((S <= 0) || (p < 0) || (n<=0))
    {
        cout << "rontome errori!\n";
        return;
    }
    if (p == 0)
    {
        cout << S / 12.0 << endl;
        return;
    }
    double r = (p / 100);
    double one_plus_r = pow((1 + r), n);
    double part_one = S * r * one_plus_r;
    double part_two = 12 * (one_plus_r - 1);
    cout << part_one / part_two << endl;
}


void credit_reverse()
{
    using namespace std;
    cout << "Enter S:\n";
    double S;
    cin >> S;
    cout << "Enter m:\n";
    double m;
    cin >> m;
    cout << "Enter n:\n";
    double n;
    cin >> n;
    if (!S || !m || !n)
    {
        cout << "rontome errori!\n";
        return;
    }
    double step = 1;
    double p = 0;
    int points_left = 6;
    int infinity_counter = 0;
    double prev_result, result, r, one_plus_r, part_one, part_two;
    result = 0;
    while (points_left)
    {
        p += step;
        r = (p / 100);
        one_plus_r = pow((1 + r), n);
        part_one = S * r * one_plus_r;
        part_two = 12 * (one_plus_r - 1);
        prev_result = result;
        result = part_one / part_two;
        if (result > m)
        {
            result = prev_result;
            p -= step;
            step *= pow(10, -1);
            points_left--;
        }
        else if (result == m) {
            break;
        }
        if (infinity_counter++ == 1000)
        {
            cout << "rontome error. endless loop";
            break;
        }
    }
    cout << p << endl;
}

void file_copy()
{
    // ������� ���� ������ ������� ���� � ���������� ��� ���������� �� �����.
    // � �������� ����� ������ ����������� �����, ������.
    using namespace std;
    string filename = "testfile";
    ifstream read_file(filename + ".txt", ios::binary);
    ofstream write_file(filename + "_copy" + ".txt", ios::binary);
    if (!read_file.is_open())
    {
        cout << "Can't open file" << endl;
        return;
    }

    cout << "COPY-ALGORITHM-C++-WAY\n";

    istreambuf_iterator<char> begin_source(read_file);
    istreambuf_iterator<char> end_source;
    ostreambuf_iterator<char> begin_dest(write_file);
    copy(begin_source, end_source, begin_dest);

    read_file.close();
    write_file.close();

    cout << "File copy operation complete\n";
}

void file_sort()
{
    using namespace std;
    ifstream read_file("testkey.txt");
    if (!read_file.is_open())
    {
        cout << "Can't open file" << endl;
        return;
    }
    while (!read_file.eof())
    {
        string number;
        read_file >> number;
        bool continueme = false;
        for (char& c : number)
            if (!isdigit(c))
            {
                continueme = true;
                break;
            }
        if (continueme) continue;
        cout << number << endl;
    }
}

void sort_string()
{
    using namespace std;
    string item;
    //char a = 'a';

    cout << "Enter string:\n";
    cin >> item;

    int* ascii = new int[item.length()];
    for (unsigned i = 0; i < item.length(); ++i)
    {
        ascii[i] = (int)item[i];
    }
    quickSort(ascii, 0, item.length() - 1);
    for (unsigned i = 0; i < item.length(); ++i)
    {
        int chr_num = ascii[i];
        if (chr_num > 32 && chr_num < 125) cout << (char)chr_num;
    }
    cout << endl;

}

void third_entry_program()
{
    using namespace std;
    cout << "Credit:\n";
    credit();
    cout << "Credit reverse:\n";
    credit_reverse();
    cout << "Copy file:\n";
    file_copy();
    cout << "File filter:\n";
    file_sort();
    cout << "Chars sort:\n";
    sort_string();
}
